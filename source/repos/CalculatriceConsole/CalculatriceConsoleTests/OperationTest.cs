﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CalculatriceConsole;

namespace CalculatriceConsoleTests
{
    [TestClass]
    public class OperationTest
    {
        [TestMethod]
        public void TestMultiplication()
        {
            Assert.AreEqual(4, Program.Multiplication(2, 2));
            Assert.AreEqual(21, Program.Multiplication(7, 3));
        }
        [TestMethod]
        public void TestAddition()
        {
            Assert.AreEqual(4, Program.Addition(2, 2));
            Assert.AreEqual(10, Program.Addition(7, 3));
        }
        [TestMethod]
        public void TestSoustraction()
        {
            Assert.AreEqual(0, Program.Soustraction(2, 2));
            Assert.AreEqual(4, Program.Soustraction(7, 3));
        }
        [TestMethod]
        public void TestDivision()
        {
            Assert.AreEqual(1, Program.Division(2, 2));
            Assert.AreEqual(2, Program.Division(8, 4));
            Assert.AreEqual(-1, Program.Division(5, 0));
            Assert.AreEqual(5, Program.Division(5, -1));
            Assert.AreEqual(5, Program.Division(-5, -1));
            Assert.IsTrue(2.3 < Program.Division(7, 3));
            Assert.IsTrue(2.34 > Program.Division(7, 3));
        }
    }
}
