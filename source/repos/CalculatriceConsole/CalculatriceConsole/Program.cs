﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droit)
        {
            return facteur_droit * facteur_gauche;
        }
        public static int Addition(int facteur_gauche, int facteur_droit)
        {
            return facteur_droit + facteur_gauche;
        }
        public static int Soustraction(int facteur_gauche, int facteur_droit)
        {
            return facteur_gauche - facteur_droit;
        }
        public static double Division(int facteur_gauche, int facteur_droit)
        {
            if (facteur_droit == 0)
            {
                return (float) -1;
            }
            facteur_droit = Math.Abs(facteur_droit);
            facteur_gauche = Math.Abs(facteur_gauche);
            return (float) facteur_gauche / facteur_droit;
        }
        static void Main(string[] args)
        {
        }
    }
}
